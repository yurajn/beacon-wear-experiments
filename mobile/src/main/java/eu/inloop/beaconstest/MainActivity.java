package eu.inloop.beaconstest;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.estimote.sdk.BeaconManager;


public class MainActivity extends Activity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks, BeaconManager.ServiceReadyCallback {

    private static final int REQUEST_ENABLE_BT = 1;

    private BeaconManager mBeaconManager;
    private boolean mServiceReady;

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));

        mBeaconManager = ((App)getApplication()).getBeaconManager();

        if (!mBeaconManager.hasBluetooth()) {
            Toast.makeText(this, "Device does not have Bluetooth Low Energy", Toast.LENGTH_LONG).show();
            finish();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!mBeaconManager.isBluetoothEnabled()) {
            Intent bt = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(bt, REQUEST_ENABLE_BT);
        } else {
            mBeaconManager.connect(this);
        }
    }

    @Override
    protected void onDestroy() {
        mBeaconManager.disconnect();
        mServiceReady = false;
        super.onDestroy();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_ENABLE_BT) {
            if (resultCode == Activity.RESULT_OK) {
                Toast.makeText(this, "Bluetooth enabled ok !", Toast.LENGTH_LONG).show();
                mBeaconManager.connect(this);
            } else {
                Toast.makeText(this, "Bluetooth not enabled, exiting...", Toast.LENGTH_LONG).show();
                finish();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }



    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments
        FragmentManager fragmentManager = getFragmentManager();
        final Fragment fragment;

        if (position == 0) {
            fragment = DistancesFragment.newInstance(mServiceReady);
        } else {
            fragment = AreasFragment.newInstance(mServiceReady);
        }

        fragmentManager.beginTransaction()
                .replace(R.id.container, fragment)
                .commit();
    }

    private Fragment getCurrentFragment() {
        return getFragmentManager().findFragmentById(R.id.container);
    }

    public void restoreActionBar() {
        ActionBar actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }

    @Override
    public void onServiceReady() {
        mServiceReady = true;

        Fragment currentFragment = getCurrentFragment();
        if (currentFragment instanceof BeaconManager.ServiceReadyCallback) {
            ((BeaconManager.ServiceReadyCallback) currentFragment).onServiceReady();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.main, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_about) {
            SpannableString spanString = new SpannableString(getString(R.string.about_text));
            Linkify.addLinks(spanString, Linkify.WEB_URLS);

            AlertDialog aboutDialog = new AlertDialog.Builder(this)
                    .setTitle(R.string.action_about)
                    .setPositiveButton(android.R.string.ok, null)
                    .setMessage(spanString)
                    .create();

            aboutDialog.show();
            ((TextView)aboutDialog.findViewById(android.R.id.message)).setMovementMethod(LinkMovementMethod.getInstance());

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
