package eu.inloop.beaconstest;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Bundle;
import android.os.RemoteException;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.estimote.sdk.Beacon;
import com.estimote.sdk.BeaconManager;
import com.estimote.sdk.Region;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class AreasFragment extends Fragment implements BeaconManager.MonitoringListener, BeaconManager.ServiceReadyCallback {

    private static final Region GREEN_BEACON = new Region("regionId", null, null, 33078);
    private static final int NOTIFICATION_ID = 1;

    private BeaconManager mBeaconManager;
    private NotificationManagerCompat mNotificationManager;
    private TextView mLabel;
    private View mMonitoringBtn;

    public static AreasFragment newInstance(boolean isServiceReady) {
        AreasFragment fragment = new AreasFragment();
        Bundle args = new Bundle();
        args.putBoolean("serviceReady", isServiceReady);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBeaconManager = ((App)getActivity().getApplication()).getBeaconManager();
        mBeaconManager.setMonitoringListener(this);

        mNotificationManager = NotificationManagerCompat.from(getActivity());

        if (getArguments() != null && getArguments().getBoolean("serviceReady")) {
            onServiceReady();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        mLabel = (TextView) rootView.findViewById(R.id.info_label);
        mMonitoringBtn = rootView.findViewById(R.id.monitoring_btn);
        mMonitoringBtn.setVisibility(View.VISIBLE);
        mMonitoringBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stopMonitoring();
            }
        });
        return rootView;
    }

    @Override
    public void onServiceReady() {
        try {
            mBeaconManager.setBackgroundScanPeriod(TimeUnit.SECONDS.toMillis(1), 0);
            mBeaconManager.startMonitoring(GREEN_BEACON);
        } catch (RemoteException e) {
            new AlertDialog.Builder(getActivity())
                    .setTitle(R.string.msg_error)
                    .setPositiveButton(android.R.string.ok, null)
                    .setMessage(e.getMessage())
                    .show();
        }
    }

    @Override
    public void onEnteredRegion(Region region, List<Beacon> beacons) {
        showNotification("Entered region");
        if (getView() != null) {
            mLabel.setText("Entered region");
            getView().setBackgroundResource(R.color.region_entered);
        }
    }

    @Override
    public void onExitedRegion(Region region) {
        showNotification("Exited region");
        if (getView() != null) {
            mLabel.setText("Exited region");
            getView().setBackgroundResource(R.color.region_exited);
        }
    }

    private void showNotification(String msg) {
        Notification notification = new Notification.Builder(getActivity())
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentTitle(msg)
                .setContentText(msg)
                .setAutoCancel(true)
                .build();
        notification.defaults |= Notification.DEFAULT_SOUND;

        mNotificationManager.notify(NOTIFICATION_ID, notification);
    }

    private void stopMonitoring() {
        try {
            mBeaconManager.stopMonitoring(GREEN_BEACON);
            mMonitoringBtn.setVisibility(View.GONE);
        } catch (RemoteException e) {
            Log.w("BeaconManager", "Could not stop ranging!");
        }
    }

    @Override
    public void onDestroy() {
        mNotificationManager.cancel(NOTIFICATION_ID);
        stopMonitoring();
        super.onDestroy();
    }
}
