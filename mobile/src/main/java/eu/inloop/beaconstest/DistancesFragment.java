package eu.inloop.beaconstest;

import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.app.AlertDialog;
import android.app.Fragment;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.RemoteException;
import android.support.annotation.ColorRes;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.estimote.sdk.Beacon;
import com.estimote.sdk.BeaconManager;
import com.estimote.sdk.Region;
import com.estimote.sdk.Utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class DistancesFragment extends Fragment implements BeaconManager.RangingListener, BeaconManager.ServiceReadyCallback {

    private static final Region ALL_BEACONS = new Region("regionId", null, null, null);

    private TextView mLabel;
    private SpannableStringBuilder mSpanBuilder = new SpannableStringBuilder();

    private SparseArray<ArrayList<Double>> mMeasuredDistances = new SparseArray<ArrayList<Double>>();
    private SparseArray<ForegroundColorSpan> mBeaconColorSpans = new SparseArray<ForegroundColorSpan>();

    private BeaconManager mBeaconManager;

    public static DistancesFragment newInstance(boolean isServiceReady) {
        DistancesFragment fragment = new DistancesFragment();
        Bundle args = new Bundle();
        args.putBoolean("serviceReady", isServiceReady);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBeaconManager = ((App)getActivity().getApplication()).getBeaconManager();
        mBeaconManager.setRangingListener(this);

        if (getArguments() != null && getArguments().getBoolean("serviceReady")) {
            onServiceReady();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        mLabel = (TextView) rootView.findViewById(R.id.info_label);
        return rootView;
    }

    @Override
    public void onBeaconsDiscovered(Region region, List<Beacon> beacons) {
        beacons = new ArrayList<Beacon>(beacons);
        Collections.sort(beacons, mBeaconComparator);

        mSpanBuilder.clear();
        mSpanBuilder.clearSpans();

        int beaconNum = 1;
        for(Beacon beacon : beacons){
            mSpanBuilder.append("\n\t");
            int spanStart = mSpanBuilder.length();
            mSpanBuilder.append("█ ")
                        .append("Beacon #").append(Integer.toString(beaconNum))
                        .append(", distance ")
                        .append(String.format("%.2fm", getMeasuredDistance(beacon)))
                        .append("\n");

            if (mBeaconColorSpans.get(beacon.getMinor()) == null) {
                int color = getResources().getColor(getColorOfBeacon(beacon));
                mBeaconColorSpans.put(beacon.getMinor(), new ForegroundColorSpan(color));
            }

            mSpanBuilder.setSpan(mBeaconColorSpans.get(beacon.getMinor()),
                    spanStart, spanStart + 1, Spanned.SPAN_INCLUSIVE_INCLUSIVE);

            if (Utils.computeProximity(beacon) == Utils.Proximity.IMMEDIATE) {
                changeBackgroundColor(getColorOfBeacon(beacon));
            }
            beaconNum++;
        }

        if (beacons.size() > 0) {
            mLabel.setText(mSpanBuilder);
        } else {
            mLabel.setText("\n\tNo beacons found\n");
        }
    }

    @Override
    public void onServiceReady() {
        try {
            mBeaconManager.startRanging(ALL_BEACONS);
        } catch (RemoteException e) {
            new AlertDialog.Builder(getActivity())
                    .setTitle(R.string.msg_error)
                    .setPositiveButton(android.R.string.ok, null)
                    .setMessage(e.getMessage())
                    .show();
        }
    }

    private void changeBackgroundColor(@ColorRes int colorRes) {
        if (getView() == null) {
            return;
        }

        ColorDrawable background = ((ColorDrawable) getView().getBackground());
        int colorOld = background == null ? 0xffffff : background.getColor();
        int colorNew = getResources().getColor(colorRes);

        ValueAnimator colorAnim = ObjectAnimator.ofInt(getView(), "backgroundColor", colorOld, colorNew);
        colorAnim.setDuration(500);
        colorAnim.setEvaluator(new ArgbEvaluator());
        colorAnim.start();
    }

    private double getMeasuredDistance(Beacon beacon) {
        //Smoothing distance values
        ArrayList<Double> prevDistOfBeacon = mMeasuredDistances.get(beacon.getMinor());
        if (prevDistOfBeacon == null) {
            prevDistOfBeacon = new ArrayList<Double>();
        }
        prevDistOfBeacon.add(0, Utils.computeAccuracy(beacon));
        mMeasuredDistances.put(beacon.getMinor(), prevDistOfBeacon);

        //Limit Max. 10 values in array
        if (prevDistOfBeacon.size() > 10) {
            prevDistOfBeacon.remove(prevDistOfBeacon.size() - 1);
        }

        double sumTotal = 0;
        for (Double aPrevDistOfBeacon : prevDistOfBeacon) {
            sumTotal += aPrevDistOfBeacon;
        }

        return sumTotal / prevDistOfBeacon.size();
    }

    private @ColorRes int getColorOfBeacon(Beacon beacon) {
        switch (beacon.getMinor()) {
            case 33078:
                return R.color.beacon_green;
            case 33938:
                return R.color.beacon_blue;
            case 36459:
                return R.color.beacon_purple;
            default:
                return 0;
        }
    }

    private final Comparator<Beacon> mBeaconComparator = new Comparator<Beacon>() {
        @Override
        public int compare(Beacon beacon, Beacon beacon2) {
            int v1 = beacon.getMinor() + beacon.getMajor();
            int v2 = beacon2.getMinor() + beacon2.getMajor();

            if (v1 > v2) {
                return 1;
            } else if (v1 < v2) {
                return -1;
            } else {
                return 0;
            }
        }
    };

    @Override
    public void onStart() {
        super.onStart();
        mMeasuredDistances.clear();
    }

    @Override
    public void onStop() {
        try {
            mBeaconManager.stopRanging(ALL_BEACONS);
        } catch (RemoteException e) {
            Log.w("BeaconManager", "Could not stop ranging!");
        }
        super.onStop();
    }

}
