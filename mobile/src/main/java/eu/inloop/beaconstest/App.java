package eu.inloop.beaconstest;

import android.app.Application;

import com.estimote.sdk.BeaconManager;

public class App extends Application {

    private BeaconManager mBeaconManager;

    @Override
    public void onCreate() {
        super.onCreate();

        mBeaconManager = new BeaconManager(this);
        com.estimote.sdk.utils.L.enableDebugLogging(true);
    }

    public BeaconManager getBeaconManager() {
        return mBeaconManager;
    }
}
